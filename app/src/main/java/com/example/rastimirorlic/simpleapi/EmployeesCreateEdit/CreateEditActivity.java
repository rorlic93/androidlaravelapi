package com.example.rastimirorlic.simpleapi.EmployeesCreateEdit;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.rastimirorlic.simpleapi.EmployeesData.EmployeesDataActivity;
import com.example.rastimirorlic.simpleapi.MainActivity;
import com.example.rastimirorlic.simpleapi.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by rastimirorlic on 04.09.2017..
 */

public class CreateEditActivity extends AppCompatActivity implements ICreateEditContract.ICreateEditView {

    private String id;
    private int type;
    private CreateEditPresenter mPresenter;

    @BindView(R.id.input_name)
    EditText inputName;
    @BindView(R.id.layout_send)
    RelativeLayout sendLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_edit);

        ButterKnife.bind(this);

        mPresenter = new CreateEditPresenter(this, this);
        type = getIntent().getIntExtra("type", 0);
        id = getIntent().getStringExtra("id");

        sendLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(inputName.getText() != null && inputName.getText().toString() != ""){
                    switch (type){
                        case 0:
                            break;
                        case 1:
                            mPresenter.createEmployee(inputName.getText().toString());
                            break;
                        case 2:
                            mPresenter.editEmployee(id, inputName.getText().toString());
                            break;
                        case 3:
                            mPresenter.createEmployee(inputName.getText().toString());
                            break;
                    }
                }
            }
        });


    }

    @Override
    public void showError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void goBack() {
        Intent intent = new Intent(this, EmployeesDataActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        Intent intent;
        if(type == 3){
            intent = new Intent(this, MainActivity.class);
        }else{
            intent = new Intent(this, EmployeesDataActivity.class);
        }

        startActivity(intent);
    }
}
