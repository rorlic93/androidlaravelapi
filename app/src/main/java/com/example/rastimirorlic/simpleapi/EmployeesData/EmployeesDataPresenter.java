package com.example.rastimirorlic.simpleapi.EmployeesData;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;

import com.example.rastimirorlic.simpleapi.API.ApiHandler;
import com.example.rastimirorlic.simpleapi.API.ServiceGenerator;
import com.example.rastimirorlic.simpleapi.Model.Employee;
import com.example.rastimirorlic.simpleapi.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rastimirorlic on 04.09.2017..
 */

public class EmployeesDataPresenter implements IEmployeesContract.IEmployeesPresenter {

    private Context mContext;
    private IEmployeesContract.IEmployeesView mView;
    private SharedPreferences mSharedPrefs;
    private Gson gson = new Gson();

    ArrayList<Employee> allEmployees = new ArrayList<>();

    public EmployeesDataPresenter(Context mContext, IEmployeesContract.IEmployeesView mView, SharedPreferences mSharedPrefs) {
        this.mContext = mContext;
        this.mView = mView;
        this.mSharedPrefs = mSharedPrefs;
    }

    @Override
    public void getEmployees() {
        ApiHandler client = ServiceGenerator.createService(ApiHandler.class, "admin@admin.hr", "admin");

        Call<ArrayList<Employee>> call = client.getEmployeesData();
        call.enqueue(new Callback<ArrayList<Employee>>() {
            @Override
            public void onResponse(Call<ArrayList<Employee>> call, Response<ArrayList<Employee>> response) {
                if(response.code() < 400){
                    saveEmployees(response);
                }else{
                    mView.showError("Nesto je poslo po krivu");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Employee>> call, Throwable t) {
                mView.showError("Nesto je poslo po krivu");
            }
        });
    }

    @Override
    public void saveEmployees(Response<ArrayList<Employee>> response) {
        allEmployees.clear();

//        String json = mSharedPrefs.getString("Employees", null);
//        ArrayList<Employee> allReadedEmployees = gson.fromJson(json, new TypeToken<ArrayList<Employee>>() {
//        }.getType());

        for(Employee employee : response.body()){
            allEmployees.add(employee);
        }

        SharedPreferences.Editor editor = mSharedPrefs.edit();

        String json = gson.toJson(allEmployees);
        editor.putString("Employees", json);
        editor.apply();




        mView.presentEmployees(allEmployees);
    }

    @Override
    public void deleteEmployee(String id) {
        ApiHandler client = ServiceGenerator.createService(ApiHandler.class, "admin@admin.hr", "admin");

        Call<ResponseBody> call = client.deleteEmployee(id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() < 400){
                    getEmployees();
                }else{
                    mView.showError("Nesto je poslo po krivu");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mView.showError("Nesto je poslo po krivu");
            }
        });
    }
}
