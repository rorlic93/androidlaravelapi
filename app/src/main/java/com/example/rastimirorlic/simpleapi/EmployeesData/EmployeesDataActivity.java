package com.example.rastimirorlic.simpleapi.EmployeesData;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.rastimirorlic.simpleapi.EmployeesCreateEdit.CreateEditActivity;
import com.example.rastimirorlic.simpleapi.MainActivity;
import com.example.rastimirorlic.simpleapi.Model.Employee;
import com.example.rastimirorlic.simpleapi.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by rastimirorlic on 04.09.2017..
 */

public class EmployeesDataActivity extends AppCompatActivity implements IEmployeesContract.IEmployeesView{

    @BindView(R.id.recycler_employees)
    RecyclerView employeesRecycler;
    @BindView(R.id.fab)
    FloatingActionButton fab;

    private EmployeesDataPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employees);

        ButterKnife.bind(this);

        mPresenter = new EmployeesDataPresenter(this, this, PreferenceManager.getDefaultSharedPreferences(this));
        mPresenter.getEmployees();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CreateEditActivity.class);
                intent.putExtra("type", 1);
                startActivity(intent);
            }
        });

    }

    @Override
    public void presentEmployees(ArrayList<Employee> allEmployees) {
        EmployeesAdapter adapter = new EmployeesAdapter(this, this, allEmployees);
        employeesRecycler.setAdapter(adapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        employeesRecycler.setHasFixedSize(true);
        employeesRecycler.setLayoutManager(layoutManager);
    }

    @Override
    public void showError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void startEditActivity(String id) {
        Intent intent = new Intent(this, CreateEditActivity.class);
        intent.putExtra("type", 3);
        intent.putExtra("id", id);
        startActivity(intent);
    }

    @Override
    public void deleteEmployee(String id) {
        mPresenter.deleteEmployee(id);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
