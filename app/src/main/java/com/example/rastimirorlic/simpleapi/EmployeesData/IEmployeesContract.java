package com.example.rastimirorlic.simpleapi.EmployeesData;

import android.support.annotation.Nullable;

import com.example.rastimirorlic.simpleapi.Model.Employee;

import java.util.ArrayList;

import retrofit2.Response;

/**
 * Created by rastimirorlic on 04.09.2017..
 */

public interface IEmployeesContract {
    interface IEmployeesView{
        void presentEmployees(ArrayList<Employee> allEmployees);

        void showError(String message);

        void startEditActivity(String id);

        void deleteEmployee(String id);
    }

    interface IEmployeesPresenter{
        void getEmployees();

        void saveEmployees(Response<ArrayList<Employee>> response);

        void deleteEmployee(String id);
    }
}
