package com.example.rastimirorlic.simpleapi.EmployeesData;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.rastimirorlic.simpleapi.Model.Employee;
import com.example.rastimirorlic.simpleapi.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by rastimirorlic on 04.09.2017..
 */

public class EmployeesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private IEmployeesContract.IEmployeesView mView;
    private ArrayList<Employee> allEmployees;

    public EmployeesAdapter(Context mContext, IEmployeesContract.IEmployeesView mView, ArrayList<Employee> allEmployees) {
        this.mContext = mContext;
        this.mView = mView;
        this.allEmployees = allEmployees;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_employee, parent, false);

        return new EmployeeView(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        EmployeeView item = (EmployeeView) holder;
        item.employeeTxt.setText(allEmployees.get(position).getImePrezime());
    }

    @Override
    public int getItemCount() {
        return allEmployees.size();
    }

    class EmployeeView extends RecyclerView.ViewHolder{

        @BindView(R.id.text_employee)
        TextView employeeTxt;
        @BindView(R.id.layout_edit)
        RelativeLayout layoutEdit;
        @BindView(R.id.layout_delete)
        RelativeLayout layoutDelete;

        public EmployeeView(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            layoutEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mView.startEditActivity(allEmployees.get(getAdapterPosition()).getId());
                }
            });

            layoutDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mView.deleteEmployee(allEmployees.get(getAdapterPosition()).getId());
                }
            });

        }

    }

}
