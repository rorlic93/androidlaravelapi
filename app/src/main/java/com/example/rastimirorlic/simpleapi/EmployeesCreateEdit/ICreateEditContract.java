package com.example.rastimirorlic.simpleapi.EmployeesCreateEdit;

import com.example.rastimirorlic.simpleapi.Model.Employee;

import java.util.ArrayList;

import retrofit2.Response;

/**
 * Created by rastimirorlic on 04.09.2017..
 */

public interface ICreateEditContract {
    interface ICreateEditView{
        void showError(String message);

        void goBack();
    }

    interface ICreateEditPresenter{
        void createEmployee(String nameToInsert);

        void editEmployee(String id, String nameToChange);

    }
}
