package com.example.rastimirorlic.simpleapi.API;

import com.example.rastimirorlic.simpleapi.Model.Employee;

import java.util.ArrayList;
import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.FieldMap;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by rastimirorlic on 04.09.2017..
 */

public interface ApiHandler {
    @GET("api/api/public/employee")
    Call<ArrayList<Employee>> getEmployeesData();

    @Multipart
    @POST("api/api/public/employee")
    Call<ResponseBody> createNewEmployee(@Part("ime_prezime") RequestBody nameToInsert);

    @Multipart
    @POST("api/api/public/employee/{id}")
    Call<ResponseBody> editEmployee(@Path("id")String id, @Part("ime_prezime") RequestBody nameToChange);

    @DELETE("api/api/public/employee/{id}")
    Call<ResponseBody> deleteEmployee(@Path("id") String id);
}
