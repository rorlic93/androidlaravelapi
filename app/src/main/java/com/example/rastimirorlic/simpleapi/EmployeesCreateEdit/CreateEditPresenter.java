package com.example.rastimirorlic.simpleapi.EmployeesCreateEdit;

import android.content.Context;

import com.example.rastimirorlic.simpleapi.API.ApiHandler;
import com.example.rastimirorlic.simpleapi.API.ServiceGenerator;
import com.example.rastimirorlic.simpleapi.Model.Employee;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rastimirorlic on 04.09.2017..
 */

public class CreateEditPresenter implements ICreateEditContract.ICreateEditPresenter {

    private Context mContext;
    private ICreateEditContract.ICreateEditView mView;

    public CreateEditPresenter(Context mContext, ICreateEditContract.ICreateEditView mView) {
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void createEmployee(String nameToInsert) {
        ApiHandler client = ServiceGenerator.createService(ApiHandler.class, "admin@admin.hr", "admin");

        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), nameToInsert);

        Call<ResponseBody> call = client.createNewEmployee(name);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() < 400){
                    mView.goBack();
                }else{
                    mView.showError("Nesto je poslo po krivu, pokusajte ponovno");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mView.showError("Nesto je poslo po krivu, pokusajte ponovno");
            }
        });
    }

    @Override
    public void editEmployee(String id, String nameToChange) {
        ApiHandler client = ServiceGenerator.createService(ApiHandler.class, "admin@admin.hr", "admin");

        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), nameToChange);

        Call<ResponseBody> call = client.editEmployee(id, name);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() < 400){
                    mView.goBack();
                }else{
                    mView.showError("Nesto je poslo po krivu, pokusajte ponovno");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mView.showError("Nesto je poslo po krivu, pokusajte ponovno");
            }
        });
    }
}
