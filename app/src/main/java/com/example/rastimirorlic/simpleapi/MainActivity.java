package com.example.rastimirorlic.simpleapi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.rastimirorlic.simpleapi.EmployeesCreateEdit.CreateEditActivity;
import com.example.rastimirorlic.simpleapi.EmployeesData.EmployeesDataActivity;
import com.facebook.stetho.Stetho;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        Stetho.initializeWithDefaults(this);
    }

    @OnClick(R.id.layout_get)
    public void getAllEmployees(){
        Intent intent = new Intent(this, EmployeesDataActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.layout_post)
    public void createNewEmployee(){
        Intent intent = new Intent(getApplicationContext(), CreateEditActivity.class);
        intent.putExtra("type", 3);
        startActivity(intent);
    }
}
